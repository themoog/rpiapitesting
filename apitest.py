from flask import Flask, request
from flask_restful import Resource, Api
from gpiozero import LED
from time import sleep
from flask_swagger_ui import get_swaggerui_blueprint

app = Flask(__name__)
api = Api(app)

### confirm led is working on startup ###
print('start......')
led = LED(3)
led.on()
led.off()



class Led(Resource):
    def get(self):
        return {'led_status' : led.value}

    def put(self):

        state = request.get_json()
        
        if state['led'] == '1':            
            led.on()
            return {'led' : state}, 201
        elif state['led'] == '0':
            led.off()
            return {'led' : state}, 201
        else:
            pass

SWAGGER_URL = "/swagger"
API_URL = "/static/swagger.json"
swaggerui_blueprint = get_swaggerui_blueprint(

    SWAGGER_URL,
    API_URL,
    config= {
        'app_name' : "Neils apitest app"
    }
)

app.register_blueprint(swaggerui_blueprint,url_prefix=SWAGGER_URL)

app.register_blueprint(request_api.get_blueprint())

        

api.add_resource(Led,'/led')

if __name__ =='__main__':   
    app.run(debug=True)
         