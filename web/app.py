from flask import Flask, request
from flask_restplus import Api, Resource, fields
#from gpiozero import LED
from time import sleep
from squid import *

app = Flask(__name__)
api = Api(app=app, version='1.0', title='Neils API Test', description='A simple LED turny on and off thingy in python running in docker!',)

### confirm led is working on startup ###

ns = api.namespace('APItest', description='Neils API test')

rgb = Squid(18, 23, 24)

print('start......')
rgb.set_color(RED)
sleep(1)
rgb.set_color(GREEN)
sleep(1)
rgb.set_color(BLUE)
sleep(1)
rgb.set_color(OFF)


@api.route("/ledon/")

class Led(Resource):

    def get(self):
                
        return {'led_status' : led.value}
        """
        Led Status
        """

    def put(self):
        
        state = request.get_json()
        
        if state['led'] == 'red':            
            rgb.set_color(RED)
            #led.on()
            return {'led' : state}, 201
        if state['led'] == 'blue':            
            rgb.set_color(BLUE)
            #led.on()
            return {'led' : state}, 201  
        if state['led'] == 'green':            
            rgb.set_color(GREEN)
            #led.on()
            return {'led' : state}, 201 
        if state['led'] == 'white':            
            rgb.set_color(WHITE)
            #led.on()
            return {'led' : state}, 201 
        
        elif state['led'] == 'off':
            rgb.set_color(OFF)
            led.off()
            return {'led' : state}, 201
        else:
            pass
        """
        Turn Led on = 1 or off = 0
        """


if __name__ =='__main__':   
      app.run(host='0.0.0.0', debug=True)
