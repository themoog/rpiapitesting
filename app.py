from flask import Flask, request
#from flask_restful import Resource, Api
from flask_restplus import Resource, Api
from gpiozero import LED
from time import sleep


# app = Flask(__name__)
#api = Api(app)


flask_app = Flask(__name__)
app = Api(app = flask_app)

name_space = app.namespace('main', description='main apis')

### confirm led is working on startup ###
print('start......')
led = LED(3)
led.on()
led.off()


@name_space.route('/led')
class Led(Resource):
    def get(self):
        return {'led_status' : led.value}

    def put(self):

        state = request.get_json()
        
        if state['led'] == '1':            
            led.on()
            return {'led' : state}, 201
        elif state['led'] == '0':
            led.off()
            return {'led' : state}, 201
        else:
            pass
        

#api.add_resource(Led,'/led')

# if __name__ =='__main__':   
#      app.run(debug=True)
         